/* SPDX-License-Identifier: Apache-2.0 OR MIT */
/*
 * Copyright (c) 2024 Rockchip Electronics Co., Ltd.
 */

#ifndef __HAL_H265D_VDPU384A_H__
#define __HAL_H265D_VDPU384A_H__

#include "mpp_hal.h"
#include "vdpu384a.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const MppHalApi hal_h265d_vdpu384a;

#ifdef __cplusplus
}
#endif

#endif /* __HAL_H265D_VDPU384A_H__ */
