/* SPDX-License-Identifier: Apache-2.0 OR MIT */
/*
 * Copyright (c) 2024 Rockchip Electronics Co., Ltd.
 */

#ifndef __VDPU384A_H__
#define __VDPU384A_H__

#define HWID_VDPU384A                (0x38436021)

#endif /* __VDPU384A_H__ */
